﻿using System.Collections;
using UnityEngine;
using Assets.Scripts;

public class ScreenManager : MonoBehaviour
{
    public GameObject winScreen;
    public GameObject loseScreen;
    public GameObject startScreen;
    private GameManager gm;
    public static GameState currentGameState;

    void Update()
    {
        gm = gameObject.GetComponentInParent<GameManager>();
        currentGameState = gm.CurrentGameState;
        switch (currentGameState)
        {
            case GameState.Start:
              
                //startScreen.SetActive(true);
                //if (Input.GetMouseButtonDown(0))
                //  {
                //    Debug.Log("SetActive(false) içinde");
                //    startScreen.SetActive(false);
                //}
                break;

            case GameState.Won:
                gm.totalDiamondScoreShownAtEndScreen.gameObject.SetActive(true);
                gm.succesfullSugarStats.gameObject.SetActive(true);
                gm.diamondScoreWithSuccesfullSugarText.gameObject.SetActive(true);
                winScreen.SetActive(true);
                startScreen.SetActive(false);
                break;

            case GameState.Lost:
                gm.totalDiamondScoreShownAtEndScreen.gameObject.SetActive(true);
                gm.succesfullSugarStats.gameObject.SetActive(true);
                gm.diamondScoreWithSuccesfullSugarText.gameObject.SetActive(true);
                loseScreen.SetActive(true);
                startScreen.SetActive(false);
                break;
            default:
                break;
        }
    }
}

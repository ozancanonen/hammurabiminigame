﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    //enums for the state of the slingshot, the 
    //state of the game and the state of the sugar
    public enum SlingshotState
    {
        
        Idle,
        OnSlingshot,
        UserPulling,
        SugarFlying
    }

    public enum GameState
    {
        Start,
        SugarMovingToSlingshot,
        Playing,
        Won,
        Lost
    }

    public enum SugarState
    {
        BeforeThrown,
        Thrown
    } 
}

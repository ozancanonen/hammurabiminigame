﻿using UnityEngine;
using Assets.Scripts;
using System;using System.Collections;

public class SlingShot : MonoBehaviour
{
    //a vector that points in the middle between left and right parts of the slingshot
    private Vector3 slingshotMiddleVector;

    [HideInInspector]
    public SlingshotState slingshotState;

    //the left and right parts of the slingshot
    public Transform leftSlingshotOrigin, rightSlingshotOrigin;

    //two line renderers to simulate the "strings" of the slingshot
    public LineRenderer slingshotLineRenderer1;
    public LineRenderer slingshotLineRenderer2;
    
    //this linerenderer will draw the projected trajectory of the thrown bird
    public LineRenderer trajectoryLineRenderer;

    //[HideInInspector]
    //the sugar to throw(who is going to slingshot and than be thrown)
    public GameObject sugarToThrow;

    //the position of the sugar tied to the slingshot
    public Transform sugarWaitPosition;
    private float throwSpeed;
    [HideInInspector]
    public float timeSinceThrown;
    private GameManager gm;

    void Start()
    {
        gm = gameObject.GetComponentInParent<GameManager>();
        //we get the ThrowSpped value from game manager inspector
        //than we divide it by 1000 to arrange it's sensitivity
        throwSpeed = gm.GetComponent<GameManager>().throwSpeed/ 1000;
    //set the sorting layer name for the line renderers
    //for the slingshot renderers this did not work so I
    //set the z on the background sprites to 10
    //hope there's a better way around that!
    slingshotLineRenderer1.sortingLayerName = "Foreground";
    slingshotLineRenderer2.sortingLayerName = "Foreground";
    trajectoryLineRenderer.sortingLayerName = "Foreground";

    slingshotLineRenderer1.SetPosition(0, leftSlingshotOrigin.position);
    slingshotLineRenderer2.SetPosition(0, rightSlingshotOrigin.position);

    slingshotLineRenderer1.SetPosition(1, leftSlingshotOrigin.position);
    slingshotLineRenderer2.SetPosition(1, rightSlingshotOrigin.position);

    //pointing at the middle position of the two vectors
    slingshotMiddleVector = new Vector3((leftSlingshotOrigin.position.x + rightSlingshotOrigin.position.x) / 2,
                                        (leftSlingshotOrigin.position.y + rightSlingshotOrigin.position.y) / 2, 0);
    }

    void Update()
    {
        
        switch (slingshotState)
        {
            case SlingshotState.Idle:
                //fix sugar's position
                if (sugarToThrow != null)
                {
                    //Animation transition bools=
                    //the animaiton where sugar flips and goes to slingshot is deactivated
                    gm.SugarsList[gm.currentSugarIndex].GetComponent<Animator>().SetBool("IsGoingToSlingshot", false);
                    //the animation where sugar stays still
                    gm.SugarsList[gm.currentSugarIndex].GetComponent<Animator>().SetBool("AtSlingshot", true);
                    //display the slingshot "strings"
                    DisplaySlingshotLineRenderers();
                    //we make the sugar's collider bigger on the slingshot so that the player can touch it easier
                    sugarToThrow.GetComponent<CircleCollider2D>().offset = new Vector2(0, 0);
                   sugarToThrow.GetComponent<CircleCollider2D>().radius = Constants.sugarColliderRadiusBig;
                    InitializeSugar();
                    slingshotState = SlingshotState.OnSlingshot;
                }
                    break;
             case SlingshotState.OnSlingshot:
                if (Input.GetMouseButtonDown(0)&& sugarToThrow != null)
                {
                    //get the point on screen user has tapped
                    Vector3 location = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    //if user has tapped onto the sugar user is pulling
                    if (sugarToThrow.GetComponent<CircleCollider2D>() == Physics2D.OverlapPoint(location))
                    {
                        sugarToThrow.GetComponent<CircleCollider2D>().radius = Constants.sugarColliderRadiusNormal;
                        slingshotState = SlingshotState.UserPulling;
                    }
                }
                break;
            case SlingshotState.UserPulling:
                DisplaySlingshotLineRenderers();
                if (Input.GetMouseButton(0))
                {
                    //get where user is tapping
                    Vector3 location = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    location.z = 0;
                    
                    if (Vector3.Distance(location, slingshotMiddleVector) > 74f)//55 was found with trial and error
                    {
                        //we will let the user pull the bird up to a maximum distance
                        var maxPosition = (location - slingshotMiddleVector).normalized * 74f + slingshotMiddleVector;
                        sugarToThrow.transform.position = maxPosition;
                    }
                    else
                    {
                        sugarToThrow.transform.position = location;
                    }
                    float distance = Vector3.Distance(slingshotMiddleVector, sugarToThrow.transform.position);
                    //display projected trajectory based on the distance
                    DisplayTrajectoryLineRenderer2(distance);
                }
                else//user has removed the tap 
                {
                    SetTrajectoryLineRenderesActive(false);
                    //throw the sugar!!!
                    timeSinceThrown = Time.time;
                    float distance = Vector3.Distance(slingshotMiddleVector, sugarToThrow.transform.position);
                    if (distance > 50)
                    {
                        SetSlingshotLineRenderersActive(false);
                        slingshotState = SlingshotState.SugarFlying;
                        ThrowSugar(distance);
                    }
                    else//user didn't pulled long enough, so reinitiate it
                    {  
                        //distance / 35 was found with trial and error 
                        //animate the bird to the wait position(positionTo command is defined in the plugins script)
                        sugarToThrow.transform.positionTo(distance / 30, //duration(found with trial and error)
                            sugarWaitPosition.transform.position). //final position
                            setOnCompleteHandler((x) =>
                        {
                            x.complete();
                            x.destroy();
                            InitializeSugar();
                        });
                    }
                }
                break;
            case SlingshotState.SugarFlying:
                break;
            default:
                break;
        }
    }

    private void ThrowSugar(float distance)
    {
        gm.numberOfSugarWeHaveThrown++; 
        gm.succesfullSugarStats.text = "Succesfull sugar rate=" + gm.sugarScore + "/" + gm.numberOfSugarWeHaveThrown;
        if (gm.SugarTrailParent.transform.childCount != 0)
        {
            Destroy(gm.SugarTrailParent.transform.GetChild(0).gameObject);
        }
        gm.gotHowMuchDiamond = 0;
        Vector3 velocity = slingshotMiddleVector - sugarToThrow.transform.position;
        sugarToThrow.GetComponent<Sugar>().OnThrow(); //make the sugar aware of it
        //(we can delete the 3 bottom lines)
        //old and alternative way
        //SugarToThrow.GetComponent<Rigidbody2D>().AddForce
        //    (new Vector2(v2.x, v2.y) * ThrowSpeed * distance * 300 * Time.deltaTime);
        //set the velocity
        sugarToThrow.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity.x, velocity.y) * throwSpeed * distance;
        //notify interested parties that the sugar was thrown
        if (SugarThrown != null)
            SugarThrown(this, EventArgs.Empty);
    }
    public event EventHandler SugarThrown;

    private void InitializeSugar()
    {
        //initialization of the ready to be thrown sugar
        //we make the sugar's collider's radius  0 so that it doesn't collide with other sugars that are jumping
        sugarToThrow.transform.position = sugarWaitPosition.position;
        SetSlingshotLineRenderersActive(true);
        slingshotState = SlingshotState.Idle;
        
    }

    void DisplaySlingshotLineRenderers()
    {
        slingshotLineRenderer1.SetPosition(1, sugarToThrow.transform.position);//burada setposition komutunda array kullanmak tanımda var
        slingshotLineRenderer2.SetPosition(1, sugarToThrow.transform.position);
    }

    void SetSlingshotLineRenderersActive(bool active)
    {
        slingshotLineRenderer1.enabled = active;
        slingshotLineRenderer2.enabled = active;
    }

    void SetTrajectoryLineRenderesActive(bool active)
    {
        trajectoryLineRenderer.enabled = active;
    }

    /// <param name="distance"></param>
    void DisplayTrajectoryLineRenderer2(float distance)
    {
        SetTrajectoryLineRenderesActive(true);
        //the vector that is from waiting posiiton to the middle of the slingshot
        Vector3 v2 = slingshotMiddleVector - sugarToThrow.transform.position; 
        int segmentCount = 50;//indicates how many points there will be in the trajectory line
                               //by changing this the length of the trajectory line will be increased
        Vector2[] segments = new Vector2[segmentCount];
        // The first line point is wherever the player's cannon, etc is
        segments[0] = sugarToThrow.transform.position;
        // The initial velocity
        Vector2 segVelocity = new Vector2(v2.x, v2.y) * throwSpeed * distance;
        for (int i = 1; i < segmentCount; i++)
        {
            float time = i * Time.fixedDeltaTime * 5;
            segments[i] = segments[0] + segVelocity * time + 0.75f * Physics2D.gravity * Mathf.Pow(time, 2);
        }
        //unity wanted to use the pragma lines of code, i don't really know what they really do to the code 
        //works without them but gives an yellow error
#pragma warning disable CS0618 // Tür veya üye eski
        trajectoryLineRenderer.SetVertexCount(segmentCount);
#pragma warning restore CS0618 // Tür veya üye eski
        for (int i = 0; i < segmentCount; i++)
            trajectoryLineRenderer.SetPosition(i, segments[i]);
    }
}

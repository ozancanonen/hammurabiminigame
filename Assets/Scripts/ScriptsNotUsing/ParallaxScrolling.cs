﻿using UnityEngine;
using System.Collections;

//this script is attached to the 3 background tree images with different parrallaxFactor written 
//to give a parallax effect when we move the camera but we don't move our camera so we don't use it
public class ParallaxScrolling : MonoBehaviour {
    public float parallaxFactor;
    Vector3 previousCameraTransform;
    private new Camera camera;

    void Start () {
        camera = Camera.main;
        previousCameraTransform = camera.transform.position;
	}
    /// <summary>
    /// similar tactics just like the "CameraMove" script
    /// </summary>

    void Update () {
        Vector3 delta = camera.transform.position - previousCameraTransform;
        delta.y = 0; delta.z = 0;
        transform.position += delta / parallaxFactor;
        previousCameraTransform = camera.transform.position;
	}
}

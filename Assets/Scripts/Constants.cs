﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public static class Constants
    {
        public static readonly float minVel = 450f;
        public static readonly float minVelocity = 2f;
        /// <summary>
        /// The collider of the sugar is bigger when on idle state
        /// This will make it easier for the user to tap on it
        /// </summary>
        public static readonly float sugarColliderRadiusNormal = 2.5f;
        public static readonly float sugarColliderRadiusBig = 10f;
    }
}

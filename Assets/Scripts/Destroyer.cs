﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {
    private GameManager gm;
    private void Start()
    {
        gm = gameObject.GetComponentInParent<GameManager>();
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        //destroyers are located in the borders of the screen
        //if the sugars collides with them, they'll destroy it
        string tag = col.gameObject.tag;
        if(tag == "Sugar" )
        {
            //we change the parent of sugars trail so that it doesn' get destroyed with sugar
            col.gameObject.transform.GetChild(0).transform.SetParent(gm.SugarTrailParent.transform);
            Destroy(col.gameObject);
        }
    }
}

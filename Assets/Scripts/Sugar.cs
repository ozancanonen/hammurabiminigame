﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class Sugar : MonoBehaviour
{
    private TextMeshProUGUI diaScore;
    public GameObject explosion;//prefab of the explosion particles
    public GameObject brickExplosion;
    private GameObject Explosion;// the act of explosion
    private GameObject BrickExplosion;
    public Sprite brokenBrickSprite;
    public Canvas canvas;
    private GameManager gm;
    DiamondScore foundDiamondScore;
    //private float sHealth;
    //private float SugarHealth;
    private bool sugarDestroyed=false;

    void Start()
    {
        //trailrenderer is not visible until we throw the bird
        GetComponentInChildren<TrailRenderer>().enabled = false;
        GetComponentInChildren<TrailRenderer>().sortingLayerName = "Foreground";
        //no gravity at first
        GetComponent<Rigidbody2D>().isKinematic = true;
        State = SugarState.BeforeThrown;
        canvas = GetComponentInParent<Canvas>();
        gm = canvas.GetComponent<GameManager>();
        //sHealth = gm.sugarHealth;
        //SugarHealth = sHealth;
        foundDiamondScore = canvas.GetComponentInChildren<DiamondScore>();
        diaScore = foundDiamondScore.GetComponent<TextMeshProUGUI>();
        diaScore.text = "x" + gm.totalDiamondScore;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>() == null) return;
        //we check if we collided with a diamond and destroy them while we add them to the  score up
        if (col.gameObject.tag == "Diamond")
        {
            gm.totalDiamondScore++;
            diaScore.text = "x" + gm.totalDiamondScore;
            gm.gotHowMuchDiamond++;
            gm.gotDiamond = true;
            Destroy(col.gameObject);
            gm.totalDiamondScoreShownAtEndScreen.text = "Total Dia score=" + gm.totalDiamondScore;
        }
        //the sugars explosion conditions and effects

        //float damage = col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 10;
        ////if the sugar collides with the cup with tagged with cup it takes damage
        //if (col.gameObject.tag == "Cup")
        //{
        //    //decrease health according to magnitude of the object that hit us
        //    sHealth -= damage;
        //    //if health is 0, destroy the block
        //    if (sHealth <= 0)
        //    {
        //        //we instantiate the explosion on where the collision takes place
        //        Explosion = Instantiate(explosion, gameObject.transform.position, Quaternion.identity);
        //        gameObject.transform.GetChild(0).transform.SetParent(gm.SugarTrailParent.transform);
        //        Destroy(gameObject);
        //        //we destroy the explosion so that it doesn't take much space in our game
        //        Destroy(Explosion, 2);
        //        //we equilize the sugar health to it's default value for the other sugar prefab that is coming
        //        sHealth = SugarHealth;
        //    }
        //    else
        //    {
        //        //we equilize the sugar health to it's default value for the other sugar prefab that is coming
        //        sHealth = SugarHealth;
        //    }
        //}
        if (col.gameObject.tag == "Brick")
        {
            
            if (gm.brickHealth<=1)
            {
                BrickExplosion = Instantiate(brickExplosion, col.gameObject.transform.position, Quaternion.identity);
                Destroy(col.gameObject);
                //we destroy the explosion so that it doesn't take much space in our game
                Destroy(BrickExplosion, 2);
            }
            else
            {
                gm.brickHealth--;
                col.gameObject.GetComponent<Image>().sprite= brokenBrickSprite;

            }
        }
    }

    void FixedUpdate()
    {
        //if we've thrown the sugar and its speed is very small we destroy it after 1 second
        if (State == SugarState.Thrown &&
            GetComponent<Rigidbody2D>().velocity.sqrMagnitude <= Constants.minVelocity&&sugarDestroyed==false)
        {
            //destroy the sugar after 1 seconds
            if (gm.SugarTrailParent.transform.childCount == 0)
            {
                //we change the parent of sugar trail so that it doesn' get destroıyed with the sugar
                if (gameObject.transform.GetChild(0) != null)
                {
                    gameObject.transform.GetChild(0).transform.SetParent(gm.SugarTrailParent.transform);
                }
            }
            StartCoroutine(DestroyAfter(5));
        }
    }

    public void OnThrow()
    {
        //play the sound
        GetComponent<AudioSource>().Play();
        //show the trail renderer
        GetComponentInChildren<TrailRenderer>().enabled = true;
        //allow for gravity forces
        GetComponent<Rigidbody2D>().isKinematic = false;
        //we change the gravity sclae to obtain a mora angular trajectory trail
         GetComponent<Rigidbody2D>().gravityScale=1.5f;
        //make the collider normal size  ... seconds afer the throw
        //GetComponent<CircleCollider2D>().radius = Constants.sugarColliderRadiusNormal;
        State = SugarState.Thrown;
    }

    IEnumerator DestroyAfter(float seconds)
    {
        sugarDestroyed = true;
        yield return new WaitForSeconds(seconds);
        Destroy(gameObject);

    }


    public SugarState State
    {
        get;
        private set;
    }
}

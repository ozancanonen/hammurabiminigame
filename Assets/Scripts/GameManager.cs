﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts;
using System.Linq;
using System.Collections;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public TextMeshProUGUI numberOfSugarsLeft;
    public TextMeshProUGUI diamondScoreWithSuccesfullSugarText;
    public TextMeshProUGUI totalDiamondScoreShownAtEndScreen;
    public TextMeshProUGUI succesfullSugarStats;
    public GameObject sugarIconForSugarLeft;
    public GameObject diamondIcon;
    //public CameraFollow cameraFollow; //we don' use camera follow fonction anymore
    [HideInInspector] public int currentSugarIndex = 0;//the sugar's number int he SugarList array to define which sugar is playing at the moment
    private int whichSugar;//the sugar's number in the SugarsList array which we will animate for jumping vertically
    public SlingShot slingshot;
    [HideInInspector] public GameState CurrentGameState = GameState.Start;
    [HideInInspector] public List<GameObject> SugarsList;//the list of all sugars 
    [HideInInspector] public List<GameObject> SugarsList2;//the list of sugars which animates the remaining sugars to jump
    [HideInInspector] public bool inScoreZone=false;//the bool which controls if the sugar is in the score zone
    [HideInInspector] public bool gotDiamond = false;//the bool which controls if the sugar collided with a diamond mid air
    [HideInInspector] public int gotHowMuchDiamond;//the variable which counts how many diamonds did the sugar collided with
    [HideInInspector] public int numberOfSugarWeHaveThrown;
    [HideInInspector] public int sugarScore;
    public GameObject scoreZone;//the GameObject which contains the scoring zone's collider
    public GameObject sugar;//sugar prefab (the gameobject that we will initialise)
    public GameObject ıronWall;
    public GameObject SugarsParent;//the parent object of all the sugar prefabs in the scene
    public GameObject SugarTrailParent;//we make the sugars trails the child of this gameobject before the sugars gets destroyed
    public GameObject bardak;
    [SerializeField] private Vector3 sugarSpawnPosition;
    private Vector3 newBardakPos;//the cups new location 
    private Vector3 defaultIronWallPos;
    private Vector3 ıronWallHeight;
    private Vector3 ıronWallShortHeight;
    private Vector3 ıronWallTallHeight; 
    public int goalSugarNumber;//scoreCounter.cs acceses to this variable
    [SerializeField]private int sugarNumber;//the number of sugar we want for player to be able to use
    public float throwSpeed;//sclinShot.cs acceses to this variable(the speed of sugar when we first throw it)
    //public float sugarHealth;//sugar.cs script acceces to this variable
    public int brickHealth;//sugar.cs script acceces to this variable for when the sugar collides with a brick
    public int diamondNumber;//diamondSpawner acceses to this variable to define how many diamonds will be crated in the scene
    public int brickNumber;//diamondSpawner acceses to this variable to define how many bricks will be created int the scene
    public int totalDiamondScore;
    private int count = 1;
    private int duration = 10;
    private bool ıronWallisNotMoving = true;


    void Start()
    {
        SummonSugars();
        StartCoroutine(JumpAnimation());
        defaultIronWallPos = ıronWall.transform.position;
        ıronWallShortHeight = defaultIronWallPos + new Vector3(0, 30, 0);
        ıronWallTallHeight = defaultIronWallPos + new Vector3(0, 60, 0);
        StartCoroutine(WallMovement());
        CurrentGameState = GameState.Start;
        //find all game ogjects with "sugar" tag than add them to the SugarsList
        SugarsList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Sugar"));
    }

    void Update()
    {
        switch (CurrentGameState)
        {
            case GameState.Start:
                //if player taps, begin animating the bird 
                //to the slingshot
                if (Input.GetMouseButtonUp(0))
                {
                    AnimateSugarToSlinghot();
                }
                break;
            case GameState.SugarMovingToSlingshot:
                break;
            case GameState.Playing:
                //if sugar is flying and (enough time had passed or sugar got destroyed) animate the other bird to slingshot

                if (slingshot.slingshotState == SlingshotState.SugarFlying &&
                     (SugarsList[currentSugarIndex] == null ||
                    (Time.time - slingshot.timeSinceThrown > 10f && inScoreZone == true)))
                    //SugarsList[currentSugarIndex].GetComponent<Rigidbody2D>().velocity.sqrMagnitude <= Constants.minVelocity
                {
                    //we count how many sugar's left in the scene
                    sugarNumber--;
                    numberOfSugarsLeft.text = "x" + sugarNumber;

                    //animate the other sugar to slingshot
                    slingshot.enabled = false;
                    StartCoroutine(WinLoseControl());
                    CurrentGameState = GameState.SugarMovingToSlingshot;
                }
                break;
            //if we have won or lost, we will restart the level
            //in a normal game, we would show the "Won" screen 
            //and on tap the user would go to the next level
            case GameState.Won:
            case GameState.Lost:
                if (Input.GetMouseButtonUp(0))
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
                break;
            default:
                break;
        }
    }
    void SummonSugars()
    {
        //makes the sugar ıcon on the left top corner visible
        sugarIconForSugarLeft.SetActive(true);
        diamondIcon.SetActive(true);
        numberOfSugarsLeft.text = "x" + sugarNumber;
        for (int i = 0; i < sugarNumber; i++)
        {
            //25 is the distance between sugar's spawn positions
            float sugarPositionChanger = i * -25;
            //creates "sugarNumber" amount of sugar prefabs in the "sugarSpawnPosition" scene with zero rotation
            GameObject newSugar = Instantiate(sugar, sugarSpawnPosition + new Vector3(sugarPositionChanger, 0f, 0f), Quaternion.identity);
            //names all of the prefabs differently so that they don't get mixed up
            sugar.name = "sugar " + i;
            //Makes all of the sugars child of "SugarsParent"
            newSugar.transform.SetParent(SugarsParent.transform);
        }
    }
    IEnumerator WallMovement()
    {
        
        while (ıronWallisNotMoving == true)
        {
            ıronWallisNotMoving = false;
             if (count <= 3)
                {
                ıronWallHeight = ıronWallShortHeight;
                duration = 10;
                }
            if (count <= 6 && count>3)
            {
                ıronWallHeight = ıronWallTallHeight;
                duration = 14;
            }
            ıronWall.transform.positionTo(Vector2.Distance(ıronWall.transform.position,
                ıronWallHeight) / duration, //duration 
                ıronWallHeight). //final position
                setOnCompleteHandler((x) =>
                    {
                        x.complete();

                        ıronWall.transform.positionTo(Vector2.Distance(ıronWall.transform.position,
                   defaultIronWallPos) / duration, //duration 
                   defaultIronWallPos). //final position
                       setOnCompleteHandler((y) =>
                       {
                       y.complete();
                       y.destroy(); //destroy the animation
               });
                        x.destroy(); //destroy the animation
                });
                yield return new WaitForSeconds(10);
                count++;
            if (count == 7)
            {
                count = 1;
            }
            ıronWallisNotMoving = true;
            }
        
    }
    IEnumerator JumpAnimation()
    {
        SugarsList2 = new List<GameObject>(GameObject.FindGameObjectsWithTag("Sugar"));
        //continue animating sugars untill there is only one left which we won't animate because it's on slingshot
        while (SugarsList2.Count > 1)
        {
            SugarsList2 = new List<GameObject>(GameObject.FindGameObjectsWithTag("Sugar"));
            //we choose which sugar we will animate randomly
            whichSugar = Random.Range(0, SugarsList2.Count);
            //so that we won't try to acces and animate an object that has been destroyed and became null
            if (SugarsList2[whichSugar] != null)
            {
                SugarsList2[whichSugar].GetComponent<Animator>().SetBool("JumpingAnimationBool", true);
                //3 seconds is one loop of jump(it is 0,6 second in real life)
                yield return new WaitForSeconds(3);
                //so that we won' get an error of trying to acces a null object
                if (SugarsList2[whichSugar] != null)
                {
                    SugarsList2[whichSugar].GetComponent<Animator>().SetBool("JumpingAnimationBool", false);
                    //we wait 1 to 2 secons before animating another sugar
                    yield return new WaitForSeconds(Random.Range(5, 10));
                }
            }
        }
    }
    IEnumerator WinLoseControl()
    {
        yield return new WaitForSeconds(2);//0,4

        //if we had been using camera follow this is where we disable it because the sugar has stopped
        //cameraFollow.IsFollowing = false;
        //when we reach the goal sugar number player has won so we pass on to the win state
        if (goalSugarNumber <= 0)
        {
            if (SugarTrailParent.transform.childCount != 0)
            {
                Destroy(SugarTrailParent.transform.GetChild(0).gameObject);
            }
            CurrentGameState = GameState.Won;
        }
        //animate the next bird, if available
        else if (currentSugarIndex == SugarsList.Count - 1)
        {
            //no more sugars,player has lost go to lost state
            if (SugarTrailParent.transform.childCount != 0)
            {
                Destroy(SugarTrailParent.transform.GetChild(0).gameObject);
            }
            CurrentGameState = GameState.Lost;
        }
        else
        {
            slingshot.slingshotState = SlingshotState.Idle;
            //sugar to throw is the next on the list
            inScoreZone = false;
            if (SugarsList[currentSugarIndex] != null)
            {
                SugarsList[currentSugarIndex].transform.SetParent(bardak.transform);
            }
            float bardakXPos = Random.Range(480,595);
            newBardakPos = new Vector3(bardakXPos, bardak.transform.position.y, bardak.transform.position.z);
            bardak.transform.positionTo(Vector2.Distance(bardak.transform.position / 10,
            newBardakPos) / 100, //duration
            newBardakPos). //final position
                setOnCompleteHandler((x) =>
                {
                    x.complete();
                    x.destroy(); //destroy the animation
                });
            if (SugarsList[currentSugarIndex] != null)
            {
                if (SugarTrailParent.transform.childCount == 0)
                {
                    SugarsList[currentSugarIndex].transform.GetChild(0).transform.SetParent(SugarTrailParent.transform);
                }
            }
            currentSugarIndex++;
            AnimateSugarToSlinghot();
        }
    }
    /// <summary>
    /// Animates the sugar from the waiting position to the slingshot
    /// </summary>
    void AnimateSugarToSlinghot()
    {
        //SugarsList[currentSugarIndex].GetComponent<Animator>().SetBool("SugarJumpAnimationBool", false);
        CurrentGameState = GameState.SugarMovingToSlingshot;
        SugarsList[currentSugarIndex].GetComponent<Animator>().SetBool("JumpingAnimationBool", false);
        SugarsList[currentSugarIndex].GetComponent<Animator>().SetBool("IsGoingToSlingshot", true);
        //positionTo is an animation that has been definded in plugins
        SugarsList[currentSugarIndex].transform.positionTo(Vector2.Distance(SugarsList[currentSugarIndex].transform.position / 10,
            slingshot.sugarWaitPosition.transform.position) / 50, //duration (old one was 40)
            slingshot.sugarWaitPosition.transform.position). //final position
                setOnCompleteHandler((x) =>
                        {
                            x.complete();
                            x.destroy(); //destroy the animation
                            CurrentGameState = GameState.Playing;
                            slingshot.enabled = true; //enable slingshot
                                                      //current sugar is the current in the list
                            slingshot.sugarToThrow = SugarsList[currentSugarIndex];
                        });
    }
}

    /// <summary>
    /// Event handler, when the bird is thrown, camera starts following it
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //private void Slingshot_BirdThrown(object sender, System.EventArgs e)
    //{
    //    cameraFollow.BirdToFollow = SugarsList[currentSugarIndex].transform;
    //    cameraFollow.IsFollowing = true;
    //}
         //use to resize the screen size
        /// <param name="screenWidth"></param>
    /// <param name="screenHeight"></param>
    //public static void AutoResize(int screenWidth, int screenHeight)
    //{
    //    Vector2 resizeRatio = new Vector2((float)Screen.width / screenWidth, (float)Screen.height / screenHeight);
    //    GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(resizeRatio.x, resizeRatio.y, 1.0f));
    //}

    //void OnGUI()
    //{
    //    AutoResize(480, 360);
    //}

﻿using UnityEngine;
using System.Collections;
using TMPro;

public class ScoreCounter : MonoBehaviour
{
    [HideInInspector] public int goalSNumber;//this one is used for calculations so it changes(when it reaches 0 game is won)
    private GameManager gm;
    public TextMeshProUGUI numberOfSugarGotInTheCup;
    private int goalSugar;//number of sugar that we suppposed to get in the cup(in the text this number is used for displaying the goal sugar)
    private int diamondScoreWithSuccesfullSugar;

    void Start()
    {  
        gm = gameObject.GetComponentInParent<GameManager>();
        //the goal sugar number is decided on game manager script on inspector than we take that value
        goalSNumber = gm.GetComponent<GameManager>().goalSugarNumber;
        goalSugar = goalSNumber;
        numberOfSugarGotInTheCup.text = gm.sugarScore + "/" + goalSugar;
    } 

    //scoreZones collider
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>() == null) return;

        if (col.gameObject.tag == "Sugar")
        {
            goalSNumber = goalSNumber - 1;
            //we change the sugars tag to untagges so that it doesn't go in to this if statement again
            col.gameObject.tag = "Untagged";
            //we slow down the inital speed to obtain the water's collission effect
            col.gameObject.GetComponent<Rigidbody2D>().velocity = col.gameObject.GetComponent<Rigidbody2D>().velocity / 6;
            col.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            //we apply a gravity in the other direction to obtain water's lifting force
            col.gameObject.GetComponent<Rigidbody2D>().gravityScale = -0.0000000001f;
            gm.inScoreZone = true;
            //col.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            //col.gameObject.GetComponent<Rigidbody2D>().simulated = false;
            gm.goalSugarNumber = goalSNumber;
            gm.sugarScore++;
            gm.succesfullSugarStats.text = "Succesfull sugar rate=" + gm.sugarScore + "/" + gm.numberOfSugarWeHaveThrown;
            //if the sugar collided with diamonds mid air we calculate how many it collided with and add it to the "diamondScoreWithSuccesfullSugar"score
            if (gm.gotDiamond==true)
            {
                diamondScoreWithSuccesfullSugar= diamondScoreWithSuccesfullSugar+gm.gotHowMuchDiamond;
                gm.gotDiamond = false;
                gm.diamondScoreWithSuccesfullSugarText.text = "Dia score with succesfull sugar=" + diamondScoreWithSuccesfullSugar;

            }
            numberOfSugarGotInTheCup.text = gm.sugarScore + "/" +goalSugar  ;

        }
    }

}

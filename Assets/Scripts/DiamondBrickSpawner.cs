﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiamondBrickSpawner : MonoBehaviour
{
    public GameObject diamond;
    public GameObject brick;
    public GameObject diamondsParent;
    public GameObject bricksParent;
    private int dNumber;
    private int bNumber;
    private GameManager gm;
    //public Vector3 diamondSpawnPosition;// old x was 400 old y was 275
    
    void Start()
    {
        //int[] DiamondYSpawnCordinates = { 275, 215, 150 };//if we choose 3 options of coordinates for diamond spawner their y cordinates are these
        gm = GetComponentInParent<GameManager>();
        dNumber = gm.diamondNumber;
        bNumber = gm.brickNumber;

        for (int i = 0; i < dNumber; i++)
        {
            //GameObject newDiamond = Instantiate(diamond, diamondSpawnPosition + new Vector3
            //(0f, DiamondYSpawnCordinates[Random.Range(1, 3)], 0f), Quaternion.identity);
            GameObject newDiamond = Instantiate(diamond, new Vector3 (Random.Range(400, 520), Random.Range(120, 250), 0f), Quaternion.identity);
            newDiamond.transform.SetParent(diamondsParent.transform);
        }
        //names all of the prefabs differently so that they don't get mixed up
        //diamond.name = "diamond " + i;
        //Makes all of the sugars child of "diamondsParent"


        for (int i = 0; i < bNumber; i++)
        {
            GameObject newBrick = Instantiate(brick, new Vector3(Random.Range(250, 450), Random.Range(120, 250), 0f), Quaternion.identity);
            newBrick.transform.SetParent(bricksParent.transform);
        }
    }
}
